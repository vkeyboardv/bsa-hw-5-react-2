import React from 'react';
import { Badge, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Form } from 'reactstrap'

const myself = 'Vladislav';

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLiked: false,
      modal: false,
      unmountOnClose: false,
      messageBody: this.props.message.message
    }

    this.toggle = this.toggle.bind(this);
  }

  // Toggle modal
  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  // Delete message
  hanldeDeleteMsg(ev) {
    this.props.deleteMsg(this.props.message.id);
  }

  // Update message
  handleUpdateMsg(ev) {
    this.toggle();
    const newObjectMessage = { ...this.props.message, message: this.state.messageBody };
    this.props.updateMsg(newObjectMessage);
  }

  render() {
    const { isLiked } = this.state;

    return (<div className={this.props.message.user === myself ? 'message myMessage' : 'message'}>
      <div className="message-avatar"><img src={this.props.message.avatar} alt="" /></div>
      <div className="message-info">
        <div className="message-info-user">{this.props.message.user}</div>
        <div className="message-info-message">{this.props.message.message}</div>
        <div className="message-info-created_at">{this.props.message.created_at}</div>
        <div className="message-info-actions">
          {this.props.message.user === myself &&
            <div className="message-info-edit-comment">
              <Form onSubmit={(e) => e.preventDefault()}>
                <Badge color="info" onClick={this.toggle}>Edit</Badge>
              </Form>
              <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} unmountOnClose={this.state.unmountOnClose}>
                <ModalHeader toggle={this.toggle}>Edit message</ModalHeader>
                <ModalBody>
                  <Input type="textarea" value={this.state.messageBody} rows={5} onChange={e => this.setState({ ...this.state, messageBody: e.target.value })} />
                </ModalBody>
                <ModalFooter>
                  <Button color="primary" onClick={ev => this.handleUpdateMsg(ev)}>Edit</Button>{' '}
                  <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                </ModalFooter>
              </Modal>
            </div>}
          <div className="message-info-is-liked">
            <Badge color="danger" onClick={ev => this.setState({ isLiked: !isLiked })}>
              {isLiked && this.props.message.user !== myself ? 1 : 0} {' '} likes
            </Badge>
          </div>
          {this.props.message.user === myself &&
            <div className="message-info-delete-comment">
              <Badge color="dark" onClick={ev => this.hanldeDeleteMsg(ev)}>X</Badge>
            </div>}
        </div>
      </div>
    </div>)
  }
}

export default Message;
