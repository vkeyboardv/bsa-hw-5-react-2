import React from 'react';
import { Spinner } from 'reactstrap';

class SpinnerElement extends React.Component {
  render() {
    return (
      <div className="spinner">
        <Spinner style={{ width: '3rem', height: '3rem' }} />{' '}
      </div>
    );
  }
}

export default SpinnerElement;
